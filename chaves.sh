#!/bin/bash
apt update
apt install -y python openssh-server
mkdir /root/.ssh
echo 'PasswordAuthentication no' >> /etc/ssh/sshd_config
echo 'PermitRootLogin without-password' >> /etc/ssh/sshd_config
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDgCQZX4cCsunBAwz9J2B/UdFWqAPvyc2uretYntB3OhvIVIwslAp2/jfF6c0ltM+0ZexJXLmcAWdeKpdxHhYajetZWf4rWZgq7LzoVc7Kq/JOkVDmQcg05gR7Zjx2YVLCIUrbuKsZ0QvdQNAKXxy3oN1kXs0OfgsKAUo/J/7Y6NHftJib8g/aGIlLWmTf5zpG+nJKZFNs9A0YofMGmNJ0ByNGhxzx/K7sv61cYqYqu9ou9MrQhxwTh2YYQR9orzP/9vw3h4cUy3C+lSdNjiAKwQGFjq14PUfDgKwxtkpeLAbGWxg8wWAJXc8sXpBp2MRi5geBvDnNOsO8eXF/VeB4FtnWzUOtxezI0Kjg9/jq2oK+OvwrzhY9ieLxgJfMioziSeRKT31MIfgixTuwuDg+pIhQ6VicG6VPJMI/tlZdTUfOBF4g2qD+lvaOG2FmVjkb1D+YHyemfVD21mT2s19rScowOCc0h9JOxnICjCAfNX8+8YDJ9sEDii2BSsM9ROk0= root@AnsiableA' > /root/.ssh/authorized_keys
service ssh restart

sudo iptables -I INPUT -p tcp -m tcp --dport 5666 -j ACCEPT
sudo iptables-save
sudo sh -c "iptables-save > /etc/iptables.rules"

#  
#chmod 0755 chaves.sh
#  ./chaves.sh
