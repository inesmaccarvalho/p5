apt update
apt install sudo wget openjdk-8-jre -y
cd /opt/
wget http://download.sonatype.com/nexus/3/nexus-3.23.0-03-unix.tar.gz
tar -xvzf nexus-3.23.0-03-unix.tar.gz
ln -s /opt/nexus-3.23.0-03/bin/nexus /etc/init.d/nexus
/etc/init.d/nexus start